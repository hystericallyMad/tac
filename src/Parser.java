import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: jmurphy
 * Date: 1/31/13
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class Parser {
    public static void main(String[] args) {
        String xmlOutFile = "C:\\Users\\jmurphy\\Desktop\\Projects\\cfr\\State Codes\\TAC\\tac20120204.xml";
        String url = "http://info.sos.state.tx.us/pls/pub/readtac$ext.viewtac";
        System.out.println("Fetching " + url);

        new DomWriter(xmlOutFile).writeXMLFile(url);

        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, url + " \nis finished processing", "Processing Complete", JOptionPane.PLAIN_MESSAGE);

        return;

    }

}
