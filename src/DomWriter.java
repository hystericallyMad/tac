import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.sql.*;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: jmurphy
 * Date: 2/5/13
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class DomWriter {
    private PrintWriter out;

    public DomWriter(String xmlOutFile) {
        try {
            out = new PrintWriter(new FileOutputStream(xmlOutFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void writeXMLFile(String url) {
        Document tacDom = null;
        LinkedList tacLL = null;
        try {
            System.out.println("Starting TAC Parsing...");
            tacLL = parseTAC(url);
            System.out.println("Finished Parsing TAC");
            System.out.println("Starting generateDOM");
            tacDom = generateDOM(tacLL);
            System.out.println("Finished generateDOM");
            System.out.println("Starting createXMLFile");
            createXMLFile(tacDom);
            System.out.println("Finished createXMLFIle");
            System.out.println("Starting loadDBTables");
            loadDBTables(tacDom);
            System.out.println("\nFinished loadDBTables");

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public LinkedList parseTAC(String url) throws IOException {
        org.jsoup.nodes.Document titleDoc = Jsoup.connect(url).get();
        Elements titleLinks = titleDoc.select("a[href]");
        LinkedList titles = new LinkedList();
        String prevURL = null, prevText = null;
        for (Element titleLink : titleLinks) {
            String s = titleLink.attr("abs:href");
            if (s.equals(prevURL)) {
                Node newNode = new Node();
                newNode.setName(titleLink.text());
                newNode.setUrl(titleLink.attr("abs:href"));
                newNode.setNum(prevText);
                titles.add(newNode);
                prevURL = titleLink.attr("abs:href");
                prevText = titleLink.text();
            } else {
                prevURL = titleLink.attr("abs:href");
                prevText = titleLink.text();
            }
        }
        /*!!Limiter!!
        *   Remove following for loop to import all titles of TAC
        *   or add additional Titles to not discard if only select Titles are needed*/
        for (int i = 0; i < titles.size(); i++) {
            Node node = (Node) titles.get(i);
            if (!node.getNum().toUpperCase().equals("TITLE 30")) {
                titles.remove(i);
                i--;
            }
        }
        prevText = null;
        prevURL = null;
        for (Object title : titles) {
            Node node = (Node) title;
            org.jsoup.nodes.Document partDoc = Jsoup.connect(node.getUrl()).get();
            Elements partLinks = partDoc.select("a[href]");
            LinkedList parts = new LinkedList();
            for (Element partLink : partLinks) {
                String s = partLink.attr("abs:href");
                if (s.equals(prevURL)) {
                    Node newNode = new Node();
                    newNode.setName(partLink.text());
                    newNode.setUrl(partLink.attr("abs:href"));
                    newNode.setNum(prevText);
                    parts.add(newNode);
                    prevURL = partLink.attr("abs:href");
                    prevText = partLink.text();
                } else {
                    prevURL = partLink.attr("abs:href");
                    prevText = partLink.text();
                }
            }
            node.setChildren(parts);
            for (Object part : parts) {
                Node partNode = (Node) part;
                org.jsoup.nodes.Document chapterDoc = Jsoup.connect(partNode.getUrl()).get();
                Elements chapterLinks = chapterDoc.select("a[href]");
                LinkedList chapters = new LinkedList();
                String prevChapURL = null, prevChapText = null;
                for (Element chapterLink : chapterLinks) {
                    String s = chapterLink.attr("abs:href");
                    if (s.equals(prevChapURL)) {
                        Node newNode = new Node();
                        newNode.setName(chapterLink.text());
                        newNode.setUrl(chapterLink.attr("abs:href"));
                        newNode.setNum(prevChapText);
                        chapters.add(newNode);
                        prevChapURL = chapterLink.attr("abs:href");
                        prevChapText = chapterLink.text();
                    } else {
                        prevChapURL = chapterLink.attr("abs:href");
                        prevChapText = chapterLink.text();
                    }
                }
                partNode.setChildren(chapters);
                String sectionSymbol = "\u00A7";
                for (Object chapter : chapters) {
                    System.out.print(".");
                    Node chapterNode = (Node) chapter;
                    org.jsoup.nodes.Document subChapDoc = Jsoup.connect(chapterNode.getUrl()).get();
                    Elements subChapLinks = subChapDoc.select("a[href]");
                    LinkedList subChaps = new LinkedList();
                    String prevSubChapURL = null, prevSubChapText = null;
                    for (Element subChapLink : subChapLinks) {
                        String s = subChapLink.attr("abs:href");
                        if (s.equals(prevSubChapURL)) {
                            Node newNode = new Node();
                            newNode.setName(subChapLink.text());
                            newNode.setUrl(subChapLink.attr("abs:href"));
                            newNode.setNum(prevSubChapText);
                            subChaps.add(newNode);
                            prevSubChapURL = subChapLink.attr("abs:href");
                            prevSubChapText = subChapLink.text();
                        } else {
                            prevSubChapURL = subChapLink.attr("abs:href");
                            prevSubChapText = subChapLink.text();
                        }
                        if (subChapLink.text().startsWith(sectionSymbol)) {
                            Node newSection = new Node();
                            newSection.setUrl(subChapLink.attr("abs:href"));
                            newSection.setName(subChapLink.parent().parent().childNode(3).childNode(0).attr("text"));
                            newSection.setNum(subChapLink.text());
                            subChaps.add(newSection);
                        }
                    }
                    chapterNode.setChildren(subChaps);
                    for (Object subChap : subChaps) {
                        Node subChapNode = (Node) subChap;
                        if (!subChapNode.getNum().startsWith(sectionSymbol)) {
                            org.jsoup.nodes.Document divisionDoc = Jsoup.connect(subChapNode.getUrl()).get();
                            Elements divisionLinks = divisionDoc.select("a[href");
                            LinkedList divisions = new LinkedList();
                            String prevDivURL = null, prevDivText = null;
                            for (Element divisionLink : divisionLinks) {
                                String s = divisionLink.attr("abs:href");
                                if (s.equals(prevDivURL)) {
                                    Node newNode = new Node();
                                    newNode.setName(divisionLink.text());
                                    newNode.setUrl(divisionLink.attr("abs:href"));
                                    newNode.setNum(prevDivText);
                                    divisions.add(newNode);
                                    prevDivURL = divisionLink.attr("abs:href");
                                    prevDivText = divisionLink.text();
                                } else {
                                    prevDivURL = divisionLink.attr("abs:href");
                                    prevDivText = divisionLink.text();
                                }
                                if (divisionLink.text().startsWith(sectionSymbol)) {
                                    Node newSection = new Node();
                                    newSection.setUrl(divisionLink.attr("abs:href"));
                                    newSection.setName(divisionLink.parent().parent().childNode(3).childNode(0).attr("text"));
                                    newSection.setNum(divisionLink.text());
                                    divisions.add(newSection);
                                }
                            }
                            subChapNode.setChildren(divisions);
                            for (Object division : divisions) {
                                Node divisionNode = (Node) division;
                                if (!divisionNode.getNum().startsWith(sectionSymbol)) {
                                    org.jsoup.nodes.Document sectionDoc = Jsoup.connect(divisionNode.getUrl()).get();
                                    Elements sectionLinks = sectionDoc.select("a[href]");
                                    LinkedList sections = new LinkedList();
                                    for (Element sectionLink : sectionLinks) {
                                        if (sectionLink.text().startsWith(sectionSymbol)) {
                                            Node newSection = new Node();
                                            newSection.setUrl(sectionLink.attr("abs:href"));
                                            newSection.setName(sectionLink.parent().parent().childNode(3).childNode(0).attr("text"));
                                            newSection.setNum(sectionLink.text());
                                            sections.add(newSection);
                                        }
                                    }
                                    divisionNode.setChildren(sections);
//                                    for(Object section:sections){
//                                        Node sectionNode = (Node)section;
//                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return titles;
    }

    public org.w3c.dom.Document generateDOM(LinkedList titles) {
        org.w3c.dom.Document outDocument = null;
        org.w3c.dom.Element currentElement = null, currentTitle = null, currentPart = null, currentChapter = null,
                currentSubChap = null, currentDivision = null, currentSection = null, number = null, name = null,
                citationElement = null;
        String sectionSymbol = "\u00A7", title = "TITLE", part = "PART", chapter = "CHAPTER",
                strSubChapter = "SUBCHAPTER", strDivision = "DIVISION", strSection = "SECTION", strNumber = "NUMBER",
                citation = "CITATION", strName = "NAME";
        String partCitation = "", chapterCitation = "", subChapterCitation = "", divisionCitation = "", sectionCitation = "";

        try {
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dFactory.newDocumentBuilder();
            outDocument = builder.newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        for (Object oTitle : titles) {
            Node titleNode = (Node) oTitle;
            currentTitle = outDocument.createElement(title);
            currentElement = currentTitle;
            outDocument.appendChild(currentTitle);
            number = outDocument.createElement(strNumber);
            currentTitle.appendChild(number);
            number.appendChild(outDocument.createTextNode(titleNode.getNum()));
            name = outDocument.createElement(strName);
            currentTitle.appendChild(name);
            name.appendChild(outDocument.createTextNode(titleNode.getName()));
            citationElement = outDocument.createElement(citation);
            currentTitle.appendChild(citationElement);
            citationElement.appendChild(outDocument.createTextNode(titleNode.getNum().substring(6) +
                    " Tex. Admin. Code."));
            partCitation = titleNode.getNum().substring(6) + " TAC ";
            chapterCitation = partCitation;
            sectionCitation = partCitation;
            if (!titleNode.getChildren().isEmpty()) {
                LinkedList parts = titleNode.getChildren();
                for (Object oPart : parts) {
                    Node partNode = (Node) oPart;
                    currentPart = outDocument.createElement(part);
                    currentTitle.appendChild(currentPart);
                    number = outDocument.createElement(strNumber);
                    currentPart.appendChild(number);
                    number.appendChild(outDocument.createTextNode(partNode.getNum()));
                    name = outDocument.createElement(strName);
                    currentPart.appendChild(name);
                    name.appendChild(outDocument.createTextNode(partNode.getName()));
                    citationElement = outDocument.createElement(citation);
                    currentPart.appendChild(citationElement);
                    citationElement.appendChild(outDocument.createTextNode(partCitation + "Part " + partNode.getNum().substring(5)));
                    if (!partNode.getChildren().isEmpty()) {
                        LinkedList chapters = partNode.getChildren();
                        for (Object oChapter : chapters) {
                            Node chapterNode = (Node) oChapter;
                            currentChapter = outDocument.createElement(chapter);
                            currentPart.appendChild(currentChapter);
                            number = outDocument.createElement(strNumber);
                            currentChapter.appendChild(number);
                            number.appendChild(outDocument.createTextNode(chapterNode.getNum()));
                            name = outDocument.createElement(strName);
                            currentChapter.appendChild(name);
                            name.appendChild(outDocument.createTextNode(chapterNode.getName()));
                            citationElement = outDocument.createElement(citation);
                            currentChapter.appendChild(citationElement);
                            citationElement.appendChild(outDocument.createTextNode(chapterCitation + "Chapter " + chapterNode.getNum().substring(8)));
                            subChapterCitation = citationElement.getTextContent() + ", ";
                            divisionCitation = subChapterCitation;
                            if (!chapterNode.getChildren().isEmpty()) {
                                LinkedList subChapters = chapterNode.getChildren();
                                for (Object subChapter : subChapters) {
                                    Node subChapterNode = (Node) subChapter;
                                    if (subChapterNode.getNum().startsWith(sectionSymbol)) {
                                        currentSection = outDocument.createElement(strSection);
                                        currentChapter.appendChild(currentSection);
                                        number = outDocument.createElement(strNumber);
                                        currentSection.appendChild(number);
                                        number.appendChild(outDocument.createTextNode(subChapterNode.getNum()));
                                        name = outDocument.createElement(strName);
                                        currentSection.appendChild(name);
                                        name.appendChild(outDocument.createTextNode(subChapterNode.getName()));
                                        citationElement = outDocument.createElement(citation);
                                        currentSection.appendChild(citationElement);
                                        citationElement.appendChild(outDocument.createTextNode(sectionCitation + subChapterNode.getNum()));
                                    } else {
                                        currentSubChap = outDocument.createElement(strSubChapter);
                                        currentChapter.appendChild(currentSubChap);
                                        number = outDocument.createElement(strNumber);
                                        currentSubChap.appendChild(number);
                                        number.appendChild(outDocument.createTextNode(subChapterNode.getNum()));
                                        name = outDocument.createElement(strName);
                                        currentSubChap.appendChild(name);
                                        name.appendChild(outDocument.createTextNode(subChapterNode.getName()));
                                        citationElement = outDocument.createElement(citation);
                                        currentSubChap.appendChild(citationElement);
                                        citationElement.appendChild(outDocument.createTextNode(subChapterCitation + subChapterNode.getNum()));
                                        if (!subChapterNode.getChildren().isEmpty()) {
                                            LinkedList divisions = subChapterNode.getChildren();
                                            for (Object division : divisions) {
                                                Node divisionNode = (Node) division;
                                                if (divisionNode.getNum().startsWith(sectionSymbol)) {
                                                    currentSection = outDocument.createElement(strSection);
                                                    currentSubChap.appendChild(currentSection);
                                                    number = outDocument.createElement(strNumber);
                                                    currentSection.appendChild(number);
                                                    number.appendChild(outDocument.createTextNode(divisionNode.getNum()));
                                                    name = outDocument.createElement(strName);
                                                    currentSection.appendChild(name);
                                                    name.appendChild(outDocument.createTextNode(divisionNode.getName()));
                                                    citationElement = outDocument.createElement(citation);
                                                    currentSection.appendChild(citationElement);
                                                    citationElement.appendChild(outDocument.createTextNode(sectionCitation + divisionNode.getNum()));
                                                } else {
                                                    currentDivision = outDocument.createElement(strDivision);
                                                    currentSubChap.appendChild(currentDivision);
                                                    number = outDocument.createElement(strNumber);
                                                    currentDivision.appendChild(number);
                                                    number.appendChild(outDocument.createTextNode(divisionNode.getNum()));
                                                    name = outDocument.createElement(strName);
                                                    currentDivision.appendChild(name);
                                                    name.appendChild(outDocument.createTextNode(divisionNode.getName()));
                                                    citationElement = outDocument.createElement(citation);
                                                    currentDivision.appendChild(citationElement);
                                                    citationElement.appendChild(outDocument.createTextNode(divisionCitation + divisionNode.getNum()));
                                                    if (!divisionNode.getChildren().isEmpty()) {
                                                        LinkedList sections = divisionNode.getChildren();
                                                        for (Object section : sections) {
                                                            Node sectionNode = (Node) section;
                                                            if (sectionNode.getNum().startsWith(sectionSymbol)) {
                                                                currentSection = outDocument.createElement(strSection);
                                                                currentDivision.appendChild(currentSection);
                                                                number = outDocument.createElement(strNumber);
                                                                currentSection.appendChild(number);
                                                                number.appendChild(outDocument.createTextNode(sectionNode.getNum()));
                                                                name = outDocument.createElement(strName);
                                                                currentSection.appendChild(name);
                                                                name.appendChild(outDocument.createTextNode(sectionNode.getName()));
                                                                citationElement = outDocument.createElement(citation);
                                                                currentSection.appendChild(citationElement);
                                                                citationElement.appendChild(outDocument.createTextNode(sectionCitation + sectionNode.getNum()));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return outDocument;
    }

    public void createXMLFile(org.w3c.dom.Document document) {
        TransformerFactory tFactory = TransformerFactory.newInstance();

        try {
            Transformer trans = tFactory.newTransformer();
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(document);
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            trans.transform(source, result);
            //System.out.println(writer.toString());
            out.println(writer.toString());
            out.close();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (TransformerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private class NodeInfo {
        int id;
        ParentType thisType;

        public NodeInfo(int id, int type) {
            this.id = id;
            switch (type) {
                case 1:
                    thisType = ParentType.Chapter;
                    break;
                case 2:
                    thisType = ParentType.SubChapter;
                    break;
                case 3:
                    thisType = ParentType.Division;
                    break;
            }
        }

        public int getNodeID() {
            return this.id;
        }

        public ParentType getNodeType() {
            return this.thisType;
        }
    }

    public void loadDBTables(org.w3c.dom.Document document) {
        NodeList titleNodeList = document.getElementsByTagName("TITLE").item(0).getChildNodes();
        int titleID = -1, partID = -1, chapterID = -1, subChapterID = -1, divisionID = -1;
        final String strNumber = "NUMBER", strName = "NAME", strPart = "PART", strChapter = "CHAPTER", strSubChap = "SUBCHAPTER",
                strDivision = "DIVISION", strSection = "SECTION", strCitation = "CITATION";
        NodeInfo chapterInfo = null, subChapterInfo = null, divisionInfo = null;
        String titleNum, partNum, chapterNum, subChapterNum, divisionNum;
        String titleName, partName, chapterName, subChapterName, divisionName;
        String titleCitation, partCitation, chapterCitation, subChapterCitation, divisionCitation;
        org.w3c.dom.Element titleNumElement = null, titleNameElement = null, titleCitationElement = null, partElement = null;
        int returnedArray[];
        for (int i = 0; i < titleNodeList.getLength(); i++) {
            if (titleNodeList.item(i).getNodeName() == strNumber) {
                titleNumElement = (org.w3c.dom.Element) titleNodeList.item(i);
            } else if (titleNodeList.item(i).getNodeName() == strName) {
                titleNameElement = (org.w3c.dom.Element) titleNodeList.item(i);
            } else if (titleNodeList.item(i).getNodeName() == strCitation) {
                titleCitationElement = (org.w3c.dom.Element) titleNodeList.item(i);
            } else if (titleNodeList.item(i).getNodeName() == strPart) {
                partElement = (org.w3c.dom.Element) titleNodeList.item(i);
            }
            if (titleNumElement != null && titleNameElement != null && titleCitationElement != null) {
                titleNum = titleNumElement.getTextContent();
                titleName = titleNameElement.getTextContent();
                titleCitation = titleCitationElement.getTextContent();
                titleID = tacTitle(titleNum, titleName, titleCitation);
                titleNumElement = null;
                titleNameElement = null;
            }
            if (partElement != null) {
                NodeList partNodeList = partElement.getChildNodes();
                org.w3c.dom.Element partNumElement = null, partNameElement = null, partCitationElement = null, chapterElement = null;
                for (int ii = 0; ii < partNodeList.getLength(); ii++) {
                    if (partNodeList.item(ii).getNodeName() == strNumber) {
                        partNumElement = (org.w3c.dom.Element) partNodeList.item(ii);
                    } else if (partNodeList.item(ii).getNodeName() == strName) {
                        partNameElement = (org.w3c.dom.Element) partNodeList.item(ii);
                    } else if (partNodeList.item(ii).getNodeName() == strCitation) {
                        partCitationElement = (org.w3c.dom.Element) partNodeList.item(ii);
                    } else if (partNodeList.item(ii).getNodeName() == strChapter) {
                        chapterElement = (org.w3c.dom.Element) partNodeList.item(ii);
                    }
                    if (partNumElement != null && partNameElement != null && partCitationElement != null) {
                        partNum = partNumElement.getTextContent();
                        partName = partNameElement.getTextContent();
                        partCitation = partCitationElement.getTextContent();
                        partID = tacPart(titleID, partNum, partName, partCitation);
                        partNumElement = null;
                        partNameElement = null;
                    }
                    if (chapterElement != null) {
                        NodeList chapterNodeList = chapterElement.getChildNodes();
                        org.w3c.dom.Element chapterNumElement = null, chapterNameElement = null, chapterCitationElement = null,
                                sectionElement = null, subChapterElement = null;
                        for (int iii = 0; iii < chapterNodeList.getLength(); iii++) {
                            if (chapterNodeList.item(iii).getNodeName() == strNumber) {
                                chapterNumElement = (org.w3c.dom.Element) chapterNodeList.item(iii);
                            } else if (chapterNodeList.item(iii).getNodeName() == strName) {
                                chapterNameElement = (org.w3c.dom.Element) chapterNodeList.item(iii);
                            } else if (chapterNodeList.item(iii).getNodeName() == strCitation) {
                                chapterCitationElement = (org.w3c.dom.Element) chapterNodeList.item(iii);
                            } else if (chapterNodeList.item(iii).getNodeName() == strSubChap) {
                                subChapterElement = (org.w3c.dom.Element) chapterNodeList.item(iii);
                            } else if (chapterNodeList.item(iii).getNodeName() == strSection) {
                                sectionElement = (org.w3c.dom.Element) chapterNodeList.item(iii);
                            }
                            if (chapterNumElement != null && chapterNameElement != null && chapterCitationElement != null) {
                                chapterNum = chapterNumElement.getTextContent();
                                chapterName = chapterNameElement.getTextContent();
                                chapterCitation = chapterCitationElement.getTextContent();
                                returnedArray = tacChapter(partID, chapterNum, chapterName, chapterCitation);
                                chapterID = returnedArray[0];
                                chapterInfo = new NodeInfo(returnedArray[0], returnedArray[1]);
                                chapterNumElement = null;
                                chapterNameElement = null;
                            }
                            if (subChapterElement != null) {
                                NodeList subChapterNodeList = subChapterElement.getChildNodes();
                                org.w3c.dom.Element subChapNumElement = null, subChapNameElement = null,
                                        subChapCitationElement = null, divisionElement = null;
                                for (int iv = 0; iv < subChapterNodeList.getLength(); iv++) {
                                    if (subChapterNodeList.item(iv).getNodeName() == strNumber) {
                                        subChapNumElement = (org.w3c.dom.Element) subChapterNodeList.item(iv);
                                    } else if (subChapterNodeList.item(iv).getNodeName() == strName) {
                                        subChapNameElement = (org.w3c.dom.Element) subChapterNodeList.item(iv);
                                    } else if (subChapterNodeList.item(iv).getNodeName() == strCitation) {
                                        subChapCitationElement = (org.w3c.dom.Element) subChapterNodeList.item(iv);
                                    } else if (subChapterNodeList.item(iv).getNodeName() == strDivision) {
                                        divisionElement = (org.w3c.dom.Element) subChapterNodeList.item(iv);
                                    } else if (subChapterNodeList.item(iv).getNodeName() == strSection) {
                                        sectionElement = (org.w3c.dom.Element) subChapterNodeList.item(iv);
                                    }
                                    if (subChapNumElement != null && subChapNameElement != null && subChapCitationElement != null) {
                                        subChapterNum = subChapNumElement.getTextContent();
                                        subChapterName = subChapNameElement.getTextContent();
                                        subChapterCitation = subChapCitationElement.getTextContent();
                                        returnedArray = tacSubChapter(chapterID, subChapterNum, subChapterName, subChapterCitation);
                                        subChapterID = returnedArray[0];
                                        subChapterInfo = new NodeInfo(returnedArray[0], returnedArray[1]);
                                        subChapNumElement = null;
                                        subChapNameElement = null;
                                    }
                                    if (divisionElement != null) {
                                        NodeList divisionNodeList = divisionElement.getChildNodes();
                                        org.w3c.dom.Element divisionNumElement = null, divisionNameElement = null,
                                                divisionCitationElement = null;
                                        for (int v = 0; v < divisionNodeList.getLength(); v++) {
                                            if (divisionNodeList.item(v).getNodeName() == strNumber) {
                                                divisionNumElement = (org.w3c.dom.Element) divisionNodeList.item(v);
                                            } else if (divisionNodeList.item(v).getNodeName() == strName) {
                                                divisionNameElement = (org.w3c.dom.Element) divisionNodeList.item(v);
                                            } else if (divisionNodeList.item(v).getNodeName() == strCitation) {
                                                divisionCitationElement = (org.w3c.dom.Element) divisionNodeList.item(v);
                                            } else if (divisionNodeList.item(v).getNodeName() == strSection) {
                                                sectionElement = (org.w3c.dom.Element) divisionNodeList.item(v);
                                            }
                                            if (divisionNumElement != null && divisionNameElement != null && divisionCitationElement != null) {
                                                divisionNum = divisionNumElement.getTextContent();
                                                divisionName = divisionNameElement.getTextContent();
                                                divisionCitation = divisionCitationElement.getTextContent();
                                                returnedArray = tacDivision(subChapterID, divisionNum, divisionName, divisionCitation);
                                                divisionID = returnedArray[0];
                                                divisionInfo = new NodeInfo(returnedArray[0], returnedArray[1]);
                                                divisionNumElement = null;
                                                divisionNameElement = null;
                                            }
                                            if (sectionElement != null) {
                                                processSectionElement(sectionElement, divisionInfo);
                                                sectionElement = null;
                                            }
                                        }
                                        divisionElement = null;
                                    }
                                    if (sectionElement != null) {
                                        processSectionElement(sectionElement, subChapterInfo);
                                        sectionElement = null;
                                    }
                                }
                                subChapterElement = null;
                            }
                            if (sectionElement != null) {
                                processSectionElement(sectionElement, chapterInfo);
                                sectionElement = null;
                            }
                        }
                        chapterElement = null;
                    }
                }
                partElement = null;
            }
        }
    }

    public void processSectionElement(org.w3c.dom.Element sectionElement, NodeInfo passedParentInfo) {
        NodeList sectionNodeList = sectionElement.getChildNodes();
        org.w3c.dom.Element sectionNumElement = null, sectionNameElement = null, sectionCitationElement = null;
        String sectionNum, sectionName, sectionCitation;
        for (int i = 0; i < sectionNodeList.getLength(); i++) {
            if (sectionNodeList.item(i).getNodeName() == "NUMBER") {
                sectionNumElement = (org.w3c.dom.Element) sectionNodeList.item(i);
            } else if (sectionNodeList.item(i).getNodeName() == "NAME") {
                sectionNameElement = (org.w3c.dom.Element) sectionNodeList.item(i);
            } else if (sectionNodeList.item(i).getNodeName() == "CITATION") {
                sectionCitationElement = (org.w3c.dom.Element) sectionNodeList.item(i);
            }
            if (sectionNumElement != null && sectionNameElement != null && sectionCitationElement != null) {
                sectionNum = sectionNumElement.getTextContent();
                sectionName = sectionNameElement.getTextContent();
                sectionCitation = sectionCitationElement.getTextContent();
                int sectionID = tacSection(passedParentInfo.getNodeID(), passedParentInfo.getNodeType(), sectionNum, sectionName, sectionCitation);
                sectionNumElement = null;
                sectionNumElement = null;
            }
        }
    }

    public enum ParentType {
        Chapter(1), SubChapter(2), Division(3);

        private int value;

        private ParentType(int value) {
            this.value = value;
        }

        public void setParentType(int value) {
            this.value = value;
        }

        public int getParentType() {
            return value;
        }
    }

    ;

    public int tacTitle(String titleNum, String titleName, String titleCitation) {
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int titleID = -1;

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT titleID FROM TAC.Title WHERE titleNum = ?");
            cs.setString(1, titleNum);
            rs = cs.executeQuery();

            while (rs.next()) {
                exist = true;
                titleID = rs.getInt("titleID");
            }

            if (exist) {
                System.out.println(titleID);
                cs = conn.prepareStatement("UPDATE TAC.Title SET titleNum = ?, titleName = ?, titleCitation = ? WHERE titleID = ?");
                cs.setString(1, titleNum);
                cs.setString(2, titleName);
                cs.setString(3, titleCitation);
                cs.setInt(4, titleID);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON TAC.TITLE TABLE (titleID: " + titleID + ")");
                    throw SQLERRUPDATE;
                }
            } else {
                cs = conn.prepareStatement("INSERT TAC.Title VALUES (?,?,?)");
                cs.setString(1, titleNum);
                cs.setString(2, titleName);
                cs.setString(3, titleCitation);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORINSERT = new Exception("ERROR INSERTING ON TAC.TITLE TABLE (titleNum: " + titleNum + ")");
                    throw SQLERRORINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 titleID FROM TAC.Title ORDER BY titleID DESC");
                rs = cs.executeQuery();
                while (rs.next()) {
                    titleID = rs.getInt("titleID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return titleID;
    }

    public int tacPart(int titleID, String partNum, String partName, String partCitation) {
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int partID = -1;
        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT partID FROM TAC.Part WHERE titleID = ? and partNum = ?");
            cs.setInt(1, titleID);
            cs.setString(2, partNum);
            rs = cs.executeQuery();
            while (rs.next()) {
                exist = true;
                partID = rs.getInt("partID");
            }
            if (exist) {
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE TAC.Part SET titleID = ?, partnum = ?, partName = ?, " +
                        "partCitation = ? WHERE partID = ?");
                cs.setInt(1, titleID);
                cs.setString(2, partNum);
                cs.setString(3, partName);
                cs.setString(4, partCitation);
                cs.setInt(5, partID);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORUPDATE = new Exception("ERROR UPDATING ON TAC.PART TABLE (partID: " + partID + ")");
                    throw SQLERRORUPDATE;
                }
            } else {
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT TAC.Part VALUES (?,?,?,?)");
                cs.setInt(1, titleID);
                cs.setString(2, partNum);
                cs.setString(3, partName);
                cs.setString(4, partCitation);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORINSERT = new Exception("ERROR INSERTING ON TAC.PART TABLE (titleID: " + titleID + " partNum: " + partNum + ")");
                    throw SQLERRORINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 partID FROM TAC.Part ORDER BY partID DESC");
                rs = cs.executeQuery();
                while (rs.next()) {
                    partID = rs.getInt("partID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }


        return partID;
    }

    public int[] tacChapter(int partID, String chapterNum, String chapterName, String chapterCitation) {
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int chapterID = -1;
        ParentType thisType = ParentType.Chapter;
        int values[] = new int[2];
        values[1] = thisType.getParentType();

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT chapterID FROM TAC.Chapter WHERE partID = ? AND chapterNum = ?");
            cs.setInt(1, partID);
            cs.setString(2, chapterNum);
            rs = cs.executeQuery();

            while (rs.next()) {
                exist = true;
                chapterID = rs.getInt("chapterID");
            }

            if (exist) {
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE TAC.Chapter SET partID = ?, chapterNum = ?, chapterName = ?, " +
                        "chapterCitation = ? WHERE chapterID = ?");
                cs.setInt(1, partID);
                cs.setString(2, chapterNum);
                cs.setString(3, chapterName);
                cs.setString(4, chapterCitation);
                cs.setInt(5, chapterID);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORUPDATE = new Exception("ERROR UPDATING ON TAC.CHAPTER TABLE (chapterID: " + chapterID + ")");
                    throw SQLERRORUPDATE;
                }
            } else {
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT TAC.Chapter VALUES (?,?,?,?)");
                cs.setInt(1, partID);
                cs.setString(2, chapterNum);
                cs.setString(3, chapterName);
                cs.setString(4, chapterCitation);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORINSERT = new Exception("ERROR INSERTING ON TAC.CHAPTER TABLE (partID: " + partID + " chapterNum: " + chapterNum + ")");
                    throw SQLERRORINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 chapterID FROM TAC.Chapter ORDER BY chapterID DESC");
                rs = cs.executeQuery();
                while (rs.next()) {
                    chapterID = rs.getInt("chapterID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        values[0] = chapterID;
        return values;
    }

    public int[] tacSubChapter(int chapterID, String subChapterNum, String subChapterName, String subChapterCitation) {
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int subChapterID = -1;
        ParentType thisType = ParentType.SubChapter;
        int values[] = new int[2];
        values[1] = thisType.getParentType();

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT subChapterID FROM TAC.subChapter WHERE chapterID = ? AND subChapterNum = ?");
            cs.setInt(1, chapterID);
            cs.setString(2, subChapterNum);
            rs = cs.executeQuery();

            while (rs.next()) {
                exist = true;
                subChapterID = rs.getInt("subChapterID");
            }

            if (exist) {
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE TAC.subChapter SET chapterID = ?, subChapterNum = ?, subChapterName = ?, " +
                        "subChapterCitation = ? WHERE subChapterID = ?");
                cs.setInt(1, chapterID);
                cs.setString(2, subChapterNum);
                cs.setString(3, subChapterName);
                cs.setString(4, subChapterCitation);
                cs.setInt(5, subChapterID);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORUPDATE = new Exception("ERROR UPDATING ON TAC.subChapter TABLE (subChapterID: " + subChapterID + ")");
                    throw SQLERRORUPDATE;
                }
            } else {
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT TAC.subChapter VALUES (?,?,?,?)");
                cs.setInt(1, chapterID);
                cs.setString(2, subChapterNum);
                cs.setString(3, subChapterName);
                cs.setString(4, subChapterCitation);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORINSERT = new Exception("ERROR INSERTING ON TAC.subChapter TABLE (chapterID: " + chapterID + " subChapterNum: " + subChapterNum + ")");
                    throw SQLERRORINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 subChapterID FROM TAC.subChapter ORDER BY subChapterID DESC");
                rs = cs.executeQuery();
                while (rs.next()) {
                    subChapterID = rs.getInt("subChapterID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        values[0] = subChapterID;
        return values;
    }

    public int[] tacDivision(int subChapterID, String divisionNum, String divisionName, String divisionCitation) {
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int divisionID = -1;
        ParentType thisType = ParentType.Division;
        int values[] = new int[2];
        values[1] = thisType.getParentType();

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT divisionID FROM TAC.division WHERE subChapterID = ? AND divisionNum = ?");
            cs.setInt(1, subChapterID);
            cs.setString(2, divisionNum);
            rs = cs.executeQuery();

            while (rs.next()) {
                exist = true;
                divisionID = rs.getInt("divisionID");
            }

            if (exist) {
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE TAC.division SET subChapterID = ?, divisionNum = ?, divisionName = ?, " +
                        "divisionCitation = ? WHERE divisionID = ?");
                cs.setInt(1, subChapterID);
                cs.setString(2, divisionNum);
                cs.setString(3, divisionName);
                cs.setString(4, divisionCitation);
                cs.setInt(5, divisionID);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORUPDATE = new Exception("ERROR UPDATING ON TAC.division TABLE (divisionID: " + divisionID + ")");
                    throw SQLERRORUPDATE;
                }
            } else {
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT TAC.division VALUES (?,?,?,?)");
                cs.setInt(1, subChapterID);
                cs.setString(2, divisionNum);
                cs.setString(3, divisionName);
                cs.setString(4, divisionCitation);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORINSERT = new Exception("ERROR INSERTING ON TAC.division TABLE (subChapterID: " + subChapterID + " divisionNum: " + divisionNum + ")");
                    throw SQLERRORINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 divisionID FROM TAC.division ORDER BY divisionID DESC");
                rs = cs.executeQuery();
                while (rs.next()) {
                    divisionID = rs.getInt("divisionID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        values[0] = divisionID;
        return values;
    }

    public int tacSection(int parentID, ParentType parentType, String sectionNum, String sectionName, String sectionCitation) {
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int sectionID = -1;

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT sectionID FROM TAC.Section WHERE parentID = ? AND parentType = ? " +
                    "AND sectionNum = ?");
            cs.setInt(1, parentID);
            cs.setInt(2, parentType.getParentType());
            cs.setString(3, sectionNum);
            rs = cs.executeQuery();

            while (rs.next()) {
                exist = true;
                sectionID = rs.getInt("sectionID");
            }
            if (exist) {
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE TAC.Section SET parentID = ?, parentType = ?, sectionNum = ?, " +
                        "sectionName = ?, sectionCitation = ? WHERE sectionID = ?");
                cs.setInt(1, parentID);
                cs.setInt(2, parentType.getParentType());
                cs.setString(3, sectionNum);
                cs.setString(4, sectionName);
                cs.setString(5, sectionCitation);
                cs.setInt(6, sectionID);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORUPDATE = new Exception("ERROR UPDATING ON TAC.SECTION TABLE (sectionID: " + sectionID + ")");
                    throw SQLERRORUPDATE;
                }
            } else {
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT TAC.Section VALUES (?,?,?,?,?)");
                cs.setInt(1, parentID);
                cs.setInt(2, parentType.getParentType());
                cs.setString(3, sectionNum);
                cs.setString(4, sectionName);
                cs.setString(5, sectionCitation);
                int i = cs.executeUpdate();
                if (i != 1) {
                    Throwable SQLERRORINSERT = new Exception("ERROR INSERTING ON LAC.SECTION TABLE (parentID: " + parentID +
                            " parentType: " + parentType.getParentType() + " sectionNum: " + sectionNum + ")");
                    throw SQLERRORINSERT;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return sectionID;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;

        String cs = "jdbc:sqlserver://localhost;databaseName=October_12_2012;";
        String user = "siteSQLAccess";
        String password = "access";
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(cs, user, password);
            System.out.print(".");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return conn;
    }
}
