import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: jmurphy
 * Date: 1/31/13
 * Time: 12:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class Node {
    private String url;
    private String num;
    private String name;
    private LinkedList children = new LinkedList();

    public LinkedList getChildren() {
        return children;
    }

    public void setChildren(LinkedList children) {
        this.children = children;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
